# sing-fastcut

This repository contains an experimental type-indexed version of a
domain model for a video editor. I started out doing this with regular
data structures in Haskell, but soon ended up covering a lot of "this
should never happen" cases, and handling `Maybe`s all over the place.
This encoding is inspired by [Stitch by Richard
Eisenberg](https://cs.brynmawr.edu/~rae/papers/2018/stitch/stitch.pdf),
and tries to capture the rules of moving a focus through a tree
structure of "groups" and "compositions" of video and audio clips.

## License

Copyright 2018 Ⓒ Oskar Wickström

[Mozilla Public License Version 2.0](LICENSE)
