{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE InstanceSigs              #-}
{-# LANGUAGE LambdaCase                #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE PolyKinds                 #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeInType                #-}
{-# LANGUAGE TypeOperators             #-}
module Main where

import           Data.Sized

import           Data.Singletons.Prelude.List
import           FastCut
import           FastCut.HList


data Command = Up | Down
  deriving (Eq, Show)

nextCommand :: IO Command
nextCommand = do
  putStrLn "Next command (u/d):"
  getLine >>= \case
    "u" -> return Up
    "d" -> return Down
    _ -> putStrLn "Invalid command." >> nextCommand

go :: FocusedSequence ft st -> IO ()
go s =
  case (focusedSequenceType s, atFocusType s) of
    (SSequenceFocusType, SGroupType (SCons _ _)) -> do
      putStrLn "At top group."
      nextCommand >>= \case
        Up -> go s
        Down -> go (applyMove MoveDownGroup s)
    (SNestedFocusType _  SSequenceFocusType, SGroupType _) -> do
      putStrLn "In nested sequence, at group."
      nextCommand >>= \case
        Up -> go (applyMove MoveUpGroup s)
        Down -> putStrLn "Nested down in group not supported yet" >> go s -- go (applyMove MoveDownGroup s)
    (_, SCompositionType{}) -> putStrLn "Stuck at composition."
    (_, SClipType{}) -> putStrLn "Stuck at clip."

main :: IO ()
main = do
  putStrLn "FastCut Singletons UI"
  go (FocusedSequence initialSequence)
  where
    initialSequence =
      Group
        (Group
           (Composition
              (Clip SVideo "1.mp4" <| Clip SVideo "2.mp4" <| empty)
              (Clip SAudio "bar.m4a" <| empty) :::
            HNil) :::
         HNil)
