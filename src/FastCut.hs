-- | Singletons version of FastCut domain model.
module FastCut
  ( module Export
  ) where

import           FastCut.Focus          as Export
import           FastCut.Movement.GADTs as Export
import           FastCut.Sequence       as Export
