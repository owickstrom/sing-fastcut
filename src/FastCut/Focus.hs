{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeInType            #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}
{-# LANGUAGE ViewPatterns          #-}

module FastCut.Focus
  ( FocusedSequence (..)
  , focusedSequenceType
  , AtFocus
  , atFocus
  , atFocusType
  , DefaultFocus
  , defaultFocus
  , module FastCut.FocusType
  ) where

import           Data.Bool               (Bool (..))
import           Data.Promotion.Prelude
import           Data.Semigroup          ((<>))
import           Data.Singletons.Prelude
import           Data.Sized
import           Data.Type.Natural
import           Data.Type.Ordinal
import           GHC.TypeLits            (ErrorMessage (..), TypeError)
import           Prelude                 (Show (..), error)

import           FastCut.FocusType
import           FastCut.HList           as HList
import           FastCut.MediaType
import           FastCut.Sequence

-- | A focused sequence is a data structures that "decorates" a
-- sequence with a valid focus.
data FocusedSequence ft st where

  -- | A nested focus, within the sequence at index 'n', on a group
  -- member or composition clip. The second argument is a function
  -- that constructs the nested focus from the sequence at index 'n'.
  NestedGroupFocus
    :: ( st ~ GroupType xs
      , nst ~ (xs `AtIndex` n)
      )
    => SNat n
    -> (Sequence nst -> FocusedSequence ft nst)
    -> Sequence st
    -> FocusedSequence (NestedFocusType n ft) st

  -- | A focused group member without any nested focus, i.e. the
  -- group member with index 'n' is focused.
  FocusedSequence
    :: Sequence st
    -> FocusedSequence SequenceFocusType st

  -- | A focused clip in a composition. The 'MediaType' variable 'mt'
  -- specifies if it's a 'Video' or 'Audio', and 'i' specifies the
  -- index.
  FocusedClip
    :: ( i < n ~ True
      , n ~ MaxClipIndex mt v a
      , SingI n
      )
    => SMediaType mt
    -> SNat i
    -> Sequence (CompositionType v a)
    -> FocusedSequence (ClipFocusType mt i) (CompositionType v a)

focusedSequenceType :: FocusedSequence ft st -> Sing ft
focusedSequenceType = \case
  NestedGroupFocus n subFocus (Group group) ->
    SNestedFocusType n (focusedSequenceType (subFocus (group `atIndex` n)))
  FocusedSequence _ -> SSequenceFocusType
  FocusedClip mt n _ -> SClipFocusType mt n

instance (Show (HList Sequence xs)) =>
         Show (FocusedSequence SequenceFocusType (GroupType xs)) where
  show =
    \case
      FocusedSequence sequence ->
        "FocusedSequence " <> show sequence

instance ( Show (HList Sequence xs)
         , Show (FocusedSequence ft (xs `AtIndex` n))
         ) =>
         Show (FocusedSequence (NestedFocusType n ft) (GroupType xs)) where
  show =
    \case
      NestedGroupFocus n toSubFocus g@(Group xs) ->
        "NestedGroupFocus " <> show n <> "" <> show (toSubFocus (xs `atIndex` n)) <>
        " " <>
        show g

instance Show (FocusedSequence (ClipFocusType mt i) (CompositionType v a)) where
  show =
    \case
      FocusedClip mediaType focus composition ->
        "FocusedClip " <> show mediaType <> " " <> show focus <> " " <>
        show composition

-- | Determines the 'Type' of the focused value.
type family AtFocus (ft :: FocusType) (st :: SequenceType) :: SequenceType where
  AtFocus SequenceFocusType st = st
  AtFocus (NestedFocusType n ft) (GroupType xs) = AtFocus ft (xs `AtIndex` n)
  AtFocus (ClipFocusType mt i) (CompositionType v a) = ClipType mt

-- | Extracts the focused value in a sequence, which can be either a
-- 'Sequence' or a 'Clip'.
atFocus :: FocusedSequence ft st -> Sequence (AtFocus ft st)
atFocus = \case
  FocusedSequence s -> s
  NestedGroupFocus n toSubFocus (Group g) ->
    atFocus (toSubFocus (g `atIndex` n))
  FocusedClip SVideo n (Composition v _) -> v %!! sNatToOrd n
  FocusedClip SAudio n (Composition _ a) -> a %!! sNatToOrd n

atFocusType :: (aft ~ AtFocus ft st) => FocusedSequence ft st -> SSequenceType aft
atFocusType s =
  case atFocus s of
    Group{}       -> SGroupType sing
    Composition{} -> SCompositionType sing sing
    Clip mt _     -> SClipType mt

-- | Group members should have a default focus. That is, when we move
-- the focus down into that sequence, we get a default focus on one of
-- /its/ children. This type family determines the default 'FocusType'
-- of a 'Sequence st'.
type family DefaultFocus (st :: SequenceType) :: FocusType where
  -- Moving the focus down into a non-empty group focuses the first
  -- group member by default.
  DefaultFocus (GroupType (x ': xs)) = NestedFocusType Z SequenceFocusType
  -- Moving the focus down into a composition where there are one or
  -- more video clips focuses the first video clip by default.
  DefaultFocus (CompositionType (S v) a) = ClipFocusType Video Z
  -- Moving the focus down into a composition where there are no video
  -- clips focuses the first audio clip by default.
  DefaultFocus (CompositionType Z (S a)) = ClipFocusType Audio Z
  -- Other cases of moving down are invalid.
  DefaultFocus s =
    TypeError (Text "No default focus available for sequence: " :<>: ShowType s)

-- | Applies the default focus to a sequence.
defaultFocus :: Sequence st -> FocusedSequence (DefaultFocus st) st
defaultFocus = \case
  group@(Group (_ ::: _)) -> NestedGroupFocus SZ FocusedSequence group
  comp@(Composition (sLength -> SS _) _) ->
    FocusedClip SVideo SZ comp
  comp@(Composition (sLength -> SZ) (sLength -> SS _)) ->
      FocusedClip SAudio SZ comp
  _ -> error "No default focus available for sequence."

-- | Type-level function that determines the maximum clip index of a
-- video or audio track.
type family MaxClipIndex (mt :: MediaType) (v :: Nat) (a :: Nat) :: Nat where
  -- TODO: Add a 'Track' kind and pass that instead?
  MaxClipIndex Video v _ = v
  MaxClipIndex Audio _ a = a
