{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeInType            #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

module FastCut.Sequence
  ( Sequence(..)
  , module FastCut.SequenceType
  , module FastCut.MediaType
  ) where

import           Data.Semigroup          ((<>))
import           Data.Singletons.Prelude
import           Data.Sized
import           Data.Vector             (Vector)
import           Prelude                 (FilePath, Show (..))

import           FastCut.HList           as HList
import           FastCut.MediaType
import           FastCut.SequenceType

-- | The indexed tree structure of the FastCut video timeline. This is
-- the structure most operations in this module work on.
data Sequence (t :: SequenceType) where

  -- | A 'Group' holds zero or more sub-sequences, and it is
  -- indexed by a corresponding type-level list.
  Group :: SingI xs => HList Sequence xs -> Sequence (GroupType xs)

  -- | A 'Composition' holds two vectors; one for the video clip track
  -- and one for the audio clip track. The track vector sizes are
  -- statically known, so that we cannot have out-of-bounds errors.
  --
  -- When rendered to video, the video track and audio track within a
  -- composition are played at the same time. Any gaps, i.e. where the
  -- other track has a longer sequence of clips, are padded with a
  -- still frame or with silence, for video and audio, respectively.
  Composition
    :: (SingI v, SingI a)
    => Sized Vector v (Sequence (ClipType Video))
    -> Sized Vector a (Sequence (ClipType Audio))
    -> Sequence (CompositionType v a)

  -- | A media clip, parameterized by its media type.
  Clip
    :: SMediaType mt
    -> FilePath
    -> Sequence (ClipType mt)

instance Show (HList Sequence xs) => Show (Sequence (GroupType xs)) where
  show (Group children) =
    "Group (" <> show children <> ")"

instance Show (Sequence (CompositionType v a)) where
  show (Composition videoClips audioClips) =
    "Composition " <> show videoClips <> " " <> show audioClips

instance Show (Sequence (ClipType mt)) where
  show (Clip mediaType path) =
    "Clip " <> show mediaType <> " " <> path
