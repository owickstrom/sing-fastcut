{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeInType            #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

-- | An approach to moving focus in a 'FocusSequence', using
-- TypeFamilies.
module FastCut.Movement.TypeFamilies where

import           Control.Category  ((.))
import           GHC.TypeLits      (ErrorMessage (..), TypeError)
import           Prelude           (error)

import           FastCut.Focus
import           FastCut.HList     as HList
import           FastCut.MediaType
import           FastCut.Sequence

-- | Determines the 'FocusType' after moving up the focus to the
-- parent sequence.
type family MovedUpFocus (ft :: FocusType) (st :: SequenceType) :: FocusType where

  MovedUpFocus (NestedFocusType n (NestedFocusType n' ft)) (GroupType xs) =
    NestedFocusType n (MovedUpFocus (NestedFocusType n' ft) (xs `AtIndex` n))

  MovedUpFocus (NestedFocusType n SequenceFocusType) (GroupType _) =
    SequenceFocusType

  MovedUpFocus (ClipFocusType mt i) (CompositionType v a) =
    SequenceFocusType

  MovedUpFocus ft ct = TypeError (Text "Cannot move up when at: " :<>: ShowType ft)

-- | Move the focus up to the parent sequence.
moveUp ::
  ( movedFocus ~ MovedUpFocus startFocus sequence
  )
  => FocusedSequence startFocus sequence
  -> FocusedSequence movedFocus sequence
moveUp =
  \case
    NestedGroupFocus n inner (Group group) ->
      case inner (group `atIndex` n) of
        NestedGroupFocus focus' inner' _ ->
          NestedGroupFocus
            n
            (moveUp . NestedGroupFocus focus' inner')
            (Group group)
        FocusedSequence {} -> FocusedSequence (Group group)
        FocusedClip {} -> error "Can't have a focused clip under nested focus."
    FocusedClip _ _ comp -> FocusedSequence comp

-- | Determines the 'FocusType' after moving the focus down.
type family MovedDownFocus (ft :: FocusType) (st :: SequenceType) :: FocusType where

  MovedDownFocus (NestedFocusType n ft) (GroupType xs) =
    NestedFocusType n (MovedDownFocus ft (xs `AtIndex` n))

  MovedDownFocus SequenceFocusType st = DefaultFocus st

  MovedDownFocus (ClipFocusType Video _) _ =
    TypeError (Text "Cannot move focus down when already focusing video clip.")

-- | Move the focus down into a child 'Sequence' or
-- 'Composition'. This operation selects a default focus.
moveDown :: FocusedSequence ft s -> FocusedSequence (MovedDownFocus ft s) s
moveDown =
  \case
    NestedGroupFocus focus subFocus group ->
      NestedGroupFocus focus (moveDown . subFocus) group
    FocusedSequence s ->
      defaultFocus s
    FocusedClip{} -> error "Cannot move focus down when focusing a clip."
