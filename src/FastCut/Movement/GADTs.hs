{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StandaloneDeriving    #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeInType            #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

-- | GADT-based approach for moving focus of a 'FocusSequence'.

module FastCut.Movement.GADTs where

import           Data.Type.Natural

import           FastCut.Focus
import           FastCut.Sequence

data Move (ft :: FocusType) (st :: SequenceType) (ft' :: FocusType) (st' :: SequenceType) where
  MoveUpGroup
    :: Move
      (NestedFocusType n nft)
      st
      SequenceFocusType
      st
  MoveDownGroup
    :: (st ~ GroupType (x : xs))
    => Move
      SequenceFocusType
      st
      (DefaultFocus st)
      st

--  MoveDownNested
--    :: Move subFocus member subFocus' member' ->
--      Move
--      (NestedFocusType n subFocus)
--      (GroupType xs)
--      (NestedFocusType n subFocus)
--      (GroupType xs)
  -- MoveDownComposition
  --   :: Move
  --     (ClipFocusType mt i)
  --     (FocusedSequence (NestedFocusType n (DefaultFocus (xs `AtIndex` n))) (GroupType xs))

applyMove :: Move ft st ft' st' -> FocusedSequence ft st -> FocusedSequence ft' st'
applyMove move s =
  case (move, s) of
    (MoveDownGroup, FocusedSequence group) ->
      NestedGroupFocus SZ FocusedSequence group
    (MoveUpGroup, NestedGroupFocus _ _ group) ->
      FocusedSequence group
    -- (MoveDownGroup, FocusedComposition focus group) ->
    --   NestedCompositionFocus focus defaultFocus group
    -- (MoveDownNested moveFurther, NestedGroupFocus focus subFocus group) ->
    --   NestedGroupFocus focus (applyMove moveFurther . subFocus) group
