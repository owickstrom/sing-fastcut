> {-# LANGUAGE DataKinds #-}
> module FastCut.Examples where

> import           Data.Sized
> import           Data.Type.Natural

> import FastCut.HList
> import FastCut

An empty group.

> emptySeq :: Sequence ('GroupType '[])
> emptySeq = Group HNil

A group containing one composition, which contains two video
clips and one audio clip.

> twoVideosOneAudio :: Sequence (GroupType '[ CompositionType (S (S Z)) (S Z)])
> twoVideosOneAudio =
>   Group
>     (Composition
>        (Clip SVideo "1.mp4" <| Clip SVideo "2.mp4" <| empty)
>        (Clip SAudio "bar.m4a" <| empty)
>      ::: HNil)

A group within a group, containing one composition, which contains no
video clips and two audio clip.

> noVideosTwoAudio ::
>      Sequence (GroupType '[ GroupType '[ CompositionType Z (S (S Z))]])
> noVideosTwoAudio =
>   Group
>   (Group
>     (Composition
>        empty
>        (Clip SAudio "1.m4a" <| Clip SAudio "2.m4a" <| empty)
>      ::: HNil)
>    ::: HNil)

We can construct a sequence focused at the second video clip of
'twoVideosOneAudio'.

> focusedAtSecondVideo
>   :: FocusedSequence
>       (NestedFocusType Z (ClipFocusType Video (S Z)))
>       (GroupType '[CompositionType (S (S Z)) (S Z)])
> focusedAtSecondVideo =
>   NestedGroupFocus
>     SZ
>     (FocusedClip SVideo (SS SZ))
>     twoVideosOneAudio

Or at the first audio clip of 'noVideosTwoAudio'.

> focusedAtFirstAudio
>   :: FocusedSequence
>       (NestedFocusType Z (NestedFocusType Z (ClipFocusType Audio Z)))
>       (GroupType '[GroupType '[CompositionType Z (S (S Z))]])
> focusedAtFirstAudio =
>   NestedGroupFocus
>     SZ
>     (NestedGroupFocus
>       SZ
>       (FocusedClip SAudio SZ))
>     noVideosTwoAudio


Use 'atFocus' to extract the focused value (either a 'Sequence' or a 'Clip').
Here we extract the focused video clip.

< >>> atFocus focusedAtSecondVideo
< Clip SVideo "2.mp4"

Moving the focus up, we can extract the entire composition.

< >>> atFocus (moveUp focusedAtSecondVideo)
< Composition [Clip SVideo "1.mp4",Clip SVideo "2.mp4"] [Clip SAudio "bar.m4a"]

We can move up to focus the composition (containing video and
audio clips), and then move back down to the default (first video
clip.) We end up "left" of where we started.

< >>> atFocus (moveDown (moveUp focusedAtSecondVideo))
< Clip SVideo "1.mp4"

If there are no video clips in the composition we move down to, we
instead get a focus on the first audio clip. Moving up and then
down on 'focusedAtFirstAudio', we end up where we started.


< >>> atFocus (moveDown (moveUp focusedAtFirstAudio))
< Clip SVideo "1.mp4"
