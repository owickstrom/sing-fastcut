{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE KindSignatures    #-}
{-# LANGUAGE PolyKinds         #-}
{-# LANGUAGE TypeFamilies      #-}
{-# LANGUAGE TypeOperators     #-}
module FastCut.HList where

import           Data.Kind
import           Data.Type.Natural
import           GHC.TypeLits      (ErrorMessage (..), TypeError)
import           Prelude

-- | A heterogeneous list data type, parameterized by some type
-- constructor `f`, and a list of `k`s, which the type constructor is
-- applied to.
data HList (f :: k -> Type) (xs :: [k]) where
  HNil :: HList f '[]
  (:::) :: f x -> HList f xs -> HList f (x ': xs)

instance Show (HList f '[]) where
  show HNil = "HNil"

instance (Show (f x), Show (HList f xs)) => Show (HList f (x ': xs)) where
  show (x ::: xs) = show x ++ " ::: " ++ show xs

-- | Returns the length of the list as a 'Nat'.
type family Length (xs :: [k]) :: Nat where
  Length '[] = Z
  Length (_ ': xs) = S (Length xs)

-- | Returns the `k` at the given index `n`.
type family AtIndex (xs :: [k]) (n :: Nat) :: k where
  AtIndex '[] _ = TypeError (Text "Out of bounds")
  AtIndex (x ': _) Z = x
  AtIndex (_ ': xs) (S n) = AtIndex xs n

-- | Returns the value at the given index `Sing n`.
atIndex :: HList f xs -> Sing n -> f (xs `AtIndex` n)
atIndex HNil _            = error "Out of bounds"
atIndex (x ::: _) SZ      = x
atIndex (_ ::: xs) (SS n) = atIndex xs n
